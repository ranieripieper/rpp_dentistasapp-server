class SiteController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  layout "blank_layout"
  
  def home
  end
  
  def home_new
    if not params[:id].nil? then
      @dentista = Dentist.where(:id => params[:id].to_i).first
    end  
  end
end
