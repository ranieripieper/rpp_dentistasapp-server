class Admin::DicaController < ApplicationController
  before_action :authenticate_user!

  def show
    @dica = Dica.find(params[:id])
  end

  def edit
    @dica = Dica.find(params[:id])
  end

  def update
    @dica = Dica.find(params[:id])

    if @dica.update(dica_params)
      flash["alert alert-success"] = "Alteração efetuada com sucesso."
      redirect_to admin_dica_path
    else
      flash["alert alert-danger"] = "Ocorreu algum erro."
      render 'edit'
    end

  end

  def new
    @dica = Dica.new
  end

  def create
    @dica = Dica.new(dica_params)

    if @dica.save
      flash["alert alert-success"] = "Categoria salva."
      redirect_to admin_dica_path
    else
      flash["alert alert-danger"] = "Ocorreu algum erro."
      render "new"
    end
  end

  def index
    @dicas = Dica.all.order_by(:_id.asc)
  end

  def destroy
    @dica = Dica.find(params[:id])
    if not @dica.nil? then
    @dica.destroy
    end
    redirect_to admin_dica_path
  end

  def dica_params
    params.require(:dica).permit(:dica, :_id, :dentista_id)
  end
end
