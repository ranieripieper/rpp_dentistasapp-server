class Admin::DentistaController < ApplicationController

  include Dentist::DentistHelper

  before_action :authenticate_user!, except: [:delete_foto]

  skip_before_filter  :verify_authenticity_token


  def show
    @dentista = Dentist.find(params[:id])
  end

  def edit
    @dentista = Dentist.find(params[:id])
    
    build_dentista(@dentista)
  end

  def update
    @dentista = Dentist.find(params[:id])

    params_update = dentista_update_params
    has_promo = params[:dentist][:has_promo]
    if has_promo.nil? or 
      (not has_promo.nil? and has_promo == "0") then
      params_update[:promocao] = nil
    end
 
    params[:dentist].delete :has_promo
    
    if not params[:dentist][:has_at_emerg].nil? and params[:dentist][:has_at_emerg] == "0" then
      params_update[:at_emerg] = nil
    end
    params[:dentist].delete :has_at_emerg
    
    if params[:dentist][:at_emerg].nil? then
      params_update[:at_emerg] = nil
    end
    
    if @dentista.update(params_update)
      
      if params[:images]
        params[:images].each { |image|
          @dentista.pictures.create(picture: image)
        }
      end
      
      flash["alert alert-success"] = "Alteração efetuada com sucesso."
      redirect_to admin_dentista_path
    else
      set_error(@dentista, "Ocorreu algum erro.")      
      render 'edit'
    end

  end

  def new
    @dentista = Dentist.new
    build_dentista(@dentista)
  end

  def create
    pwd = SecureRandom.hex(8)
    params[:dentist][:password] = pwd
    params[:dentist]["password_confirmation"] = pwd
    
    if not params[:dentist][:has_promo].nil? and params[:dentist][:has_promo] == "0" then
      params[:dentist].delete :promocao
    end
    params[:dentist].delete :has_promo
    
    if not params[:dentist][:has_at_emerg].nil? and params[:dentist][:has_at_emerg] == "0" then
      params[:dentist].delete :at_emerg
    end
    params[:dentist].delete :has_at_emerg
    
    if params[:dentist][:at_emerg].nil? then
      params[:dentist][:at_emerg] = []
    end

    @dentista = Dentist.new(dentista_create_params)
    if @dentista.save
      #@dentista.pictures = []
      if params[:images]
        params[:images].each { |image|
          @dentista.pictures.create(picture: image)
        }
        #@dentista.save
      end
      
      flash["alert alert-success"] = "Alteração efetuada com sucesso."
      redirect_to admin_dentista_path
    else
      set_error(@dentista, "Ocorreu algum erro.")    
      build_dentista(@dentista) 
      render 'new'
    end
  end

  def index
    @dentistas = Dentist.unscoped.all.order_by(:id.asc)
  end

  def destroy
    @dentista = Dentist.find(params[:id])
    if not @dentista.nil? then
    @dentista.destroy
    end
    redirect_to admin_dentista_path
  end
  
  def delete_foto
    #verifica se o usuário ou dentista está logado
    if user_signed_in? or dentist_signed_in? then
      if dentist_signed_in? then
        @dentista = currrent_dentist
      else
        @dentista = Dentist.find(params[:id])
      end
      
      if delete_foto_helper(@dentista, params[:id_image].to_i) then
        flash["alert alert-success"] = "Foto deletada com sucesso."
      else
        set_error(@dentista, "Ocorreu algum erro.")
      end
      
      if user_signed_in? then
         redirect_to admin_dentista_edit_path(@dentista)
      else
         redirect_to edit_dentist_registration_path(@dentista)
      end
    else
      flash["alert alert-success"] = "Ocorreu algum erro."
      redirect_to root_path
    end
  end

  def dentista_update_params
    params.require(:dentist).permit(:active, :dt_expiration_date, :_id, :nome, :site, :email, :profile, :icon, :destaque, :phones => [], :equipe => [], :servicos => [], #:at_normal => [], 
    address: [:street, :number, :complement, :neighborhood, :zip_code, :city, :state], 
    promocao: [:perc, :servico], at_normal: [:dia_sem, :h_open, :h_close], at_emerg: [:dia_sem, :h_open, :h_close])
  end
  
  def dentista_create_params
    params.require(:dentist).permit(:active, :dt_expiration_date, :password, "password_confirmation", :_id, :nome, :site, :email, :profile, :icon, :destaque, :phones => [], :equipe => [], :servicos => [], 
    address: [:street, :number, :complement, :neighborhood, :zip_code, :city, :state], promocao: [:perc, :servico], at_normal: [:dia_sem, :h_open, :h_close], at_emerg: [:dia_sem, :h_open, :h_close])
  end
  
  private
    def get_photo_filename(file_name)
      extn = File.extname  file_name 
      return SecureRandom.uuid + extn
    end
    
    def get_foto_file_name(uploaded_io)
      file_name = nil
      if not uploaded_io.nil? then
        img = MiniMagick::Image.open(uploaded_io.path)
        puts "******** >>>> #{img[:width]} x  #{img[:height]}"
        file_name = get_photo_filename(uploaded_io.original_filename)     
      end
      
      if not uploaded_io.nil? then
        #File.open(Rails.root.join('public', 'upload', "#{file_name}"), 'wb') do |file|
        directory = MainYetting.PATH_UPLOAD_IMGS
        File.open(File.join(directory, "#{file_name}"), 'wb') do |file|
          file.write(uploaded_io.read)
        end
        return "/upload/#{file_name}"  
      end
      
      return nil
           
    end
    
    def set_error(obj, msg)
      if not obj.errors.nil? then
        flash["alert alert-danger"] = obj.errors.full_messages
      else
        flash["alert alert-danger"] = msg
      end
    end
    
    def build_dentista(dentista)
      if dentista.address.nil? then
        dentista.address = Address.new
      end
      if dentista.promocao.nil? then
        dentista.promocao = Promocao.new
      end
      if dentista.at_normal.nil? or dentista.at_normal.size <= 0  then
        dentista.at_normal.build
      end
    
      if dentista.at_emerg.nil? or dentista.at_emerg.size <= 0  then
        dentista.at_emerg.build
      end
    end
end
