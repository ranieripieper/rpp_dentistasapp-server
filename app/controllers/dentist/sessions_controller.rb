class Dentist::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]
 before_filter :sign_out_pre_login, only: [:new]

  layout :resolve_layout

  def resolve_layout
    case action_name
    when "new"
      "blank_layout"
    else
      "application"
    end
  end
  

  def edit     
    flash.clear
    super
  end
  # GET /resource/sign_in
  def new     
    sign_out(current_dentist)
    super
  end

  def sign_out_pre_login
    sign_out(current_dentist)
  end
  def after_sign_in_path_for(resource)
    edit_dentist_registration_path(resource)
  end

  # POST /resource/sign_in
  def create
    super
    if not dentist_signed_in? then
      flash["alert alert-danger"] = I18n.t(:error_default_msg)
    else
      #puts ">>>>>> signed"
    end
  end

  def destroy
    super
    redirect_to root_path
  end
  
  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # You can put the params you want to permit in the empty array.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end
