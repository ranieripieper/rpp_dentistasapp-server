class Dentist::PaymentController < ApplicationController
  include Dentistas::PaymentHelpers
 
  layout :resolve_layout

  skip_before_filter :verify_authenticity_token, :only => :update_status

  #force_ssl only: [:redo_payment,:process_redo_payment]

  def resolve_layout
    case action_name
    when "process_payment"
      "application"
    else
      "blank_layout"
    end
  end
  
  def redo_payment
    @dentista = Dentist.find(params[:dentista_id])
  end
  
  def process_redo_payment
    @dentista = Dentist.find(params[:dentist][:dentista_id])
    if @dentista.update(redo_params)
      value = Dentistas::PaymentHelpers::get_value(@dentista.plano)
      payment(@dentista, value, params)
      redirect_to root_path(:msg => "redo Pagamento")
    else
      flash["alert alert-danger"] = I18n.t(:error_default_msg)
      redirect_to redo_payment_path(@dentista)
    end
    
  end
  
  def update_status
    transaction_id = params[:id]
    status = params[:current_status]
    if not transaction_id.nil? then
      payment = Payment.where(:transaction_id => transaction_id).first
      #faz update do status
      payment_status = PaymentStatus.new
      payment_status.dt = Time.now
      payment_status.status = status # status da transação
      payment.payment_status << payment_status
      payment.save
      @dentista = Dentist.find(payment.dentist_id)
      if status == MainYetting.PAYMENT_STATUS_PAID then
        if @dentista.plano == MainYetting.PLANO_ANUAL then
          @dentista.dt_expiration_date = Date.today() + 12.months
        else
          @dentista.dt_expiration_date = Date.today() + 6.months
        end

        @dentista.save
        send_access_email(@dentista, payment)
      elsif status == MainYetting.PAYMENT_STATUS_REFUSED
        
        send_refused_status(@dentista, payment, status)
      else
        #puts "**** send_update_status #{@dentista.id}"
        #send_update_status(@dentista, payment, status)
      end
    end
  end
  
  def send_refused_status(dentista, payment, status)
    Thread.new do
      DentistaMailer.payment_refused(dentista, payment, status).deliver!
    end
  end
  
  def send_update_status(dentista, payment, status)
    Thread.new do
      DentistaMailer.update_status(dentista, payment, status).deliver!
    end
  end

  def process_payment
    id = params[:dentista_id]
    if not id.nil? then
      @dentista = Dentist.find(id)
           
      Thread.new do
        payment = Payment.where(:dentist_id => @dentista.id).last
        DentistaMailer.payment_process(@dentista, payment).deliver!
      end
    
    end
  end
  
private

  def redo_params
    params.require(:dentist).permit(:plano)
  end
  
  def send_access_email(dentista, payment)
    generated_password = Devise.friendly_token.first(8)
    dentista.password = generated_password

    Thread.new do
      DentistaMailer.payment_confirmed(dentista, generated_password).deliver!
    end
    dentista.save
  end

end
