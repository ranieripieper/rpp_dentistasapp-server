class Dentist::RegistrationsController < Devise::RegistrationsController
# before_filter :configure_sign_up_params, only: [:create]
# before_filter :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end
  
  #force_ssl only: [:new,:create]
  
  skip_before_filter  :verify_authenticity_token
  
  layout :resolve_layout

  def resolve_layout
    case action_name
    when "new", "edit", "create", "update"
      "blank_layout"
    else
      "application"
    end
  end

  include Dentistas::PaymentHelpers

  def after_sign_up_path_for(resource)
    root_path(:msg => "Cadastro efetuado")
  end

  def after_update_path_for(resource)
    edit_dentist_registration_path(resource)
  end
    
  def edit
    flash.clear
    super    
  end
  
  def new
    flash.clear
    super
  end
  
  # POST /resource
  def create
    pwd = SecureRandom.hex(8)
    params[:dentist][:password] = pwd
    params[:dentist]["password_confirmation"] = pwd

    build_resource(sign_up_params) 
    flash.clear
    
    if params["accept_termos"].nil? or params["accept_termos"] != "on" then
      flash["alert alert-danger"] = I18n.t(:error_termos_de_uso_nao_aceito)
      render "new"
    else
      super
      if resource.errors.nil? or resource.errors.empty? then
        value = Dentistas::PaymentHelpers::get_value(resource.plano)
        payment(resource, value, params)
        Thread.new do
          payment = Payment.where(:dentist_id => resource.id).last
          DentistaMailer.payment_process(resource, payment).deliver!
        end
        sign_out
      else
        #erro cadastro não realizado
      end
     end
  end

  def edit
    @dentista = current_dentist
    build_dentista(@dentista)
    super
    flash.clear  
    
  end

  def build_dentista(dentista)
      if dentista.address.nil? then
        dentista.address = Address.new
      end
      if dentista.promocao.nil? then
        dentista.promocao = Promocao.new
      end
      if dentista.at_normal.nil? or dentista.at_normal.size <= 0  then
        dentista.at_normal.build
      end
    
      if dentista.at_emerg.nil? or dentista.at_emerg.size <= 0  then
        dentista.at_emerg.build
      end
    end


  def dentista_params
    params.require(:dentist).permit(:_id, :nome, :site, :email, :profile, :icon, :destaque, :phones => [], :equipe => [], :servicos => [],
    address: [:street, :number, :complement, :neighborhood, :zip_code, :city, :state], promocao: [:perc, :servico], at_normal: [:dia_sem, :h_open, :h_close], at_emerg: [:dia_sem, :h_open, :h_close])
  end
  
  def update
    @dentista = current_dentist
    
    params_update = dentista_params
    has_promo = params[:dentist][:has_promo]
    if has_promo.nil? or 
      (not has_promo.nil? and has_promo == "0") then
      params_update[:promocao] = nil
    end
    
    params[:dentist].delete :has_promo
    
    if not params[:dentist][:has_at_emerg].nil? and params[:dentist][:has_at_emerg] == "0" then
      params_update[:at_emerg] = nil
    end
    params[:dentist].delete :has_at_emerg
    
    if params[:dentist][:at_emerg].nil? then
      params_update[:at_emerg] = nil
    end

    if @dentista.dt_expiration_date >= Date.today() and 
      not @dentista.address.nil? and not @dentista.address.coordinates.nil?  and 
      not @dentista.address.coordinates.empty? then
      params_update[:active] = true
    else
      params_update[:active] = false
    end

    if @dentista.update(params_update)
      
      if params[:images]
        #===== The magic is here ;)
        params[:images].each { |image|
          @dentista.pictures.create(picture: image)
        }
      end
      
      flash["alert alert-success"] = "Alteração efetuada com sucesso."
    else
      set_error(@dentista, "Ocorreu algum erro.")
    end

    build_dentista(@dentista)
    render 'edit'
  end
  
  def sign_up_params
    params.require(:dentist).permit(:nome, :email, :plano, :password, :password_confirmation, :phones => [])
  end
end
