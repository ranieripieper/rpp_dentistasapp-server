class Api::V1::DicasController < ApplicationController

  respond_to :json
  
  #Retorna dados da dica
  # http://dentistasdds.dev.com/api/v1/dentista/545a5e0c52616e29a8010000.json
  def get
    dica_id = params[:dica_id]
    if dica_id.nil? or dica_id.empty? then
        respond_to do |format|
          format.json { render :json => {:success => false,
                                         :info => "Parâmetros inválidos!"} }
        end
    else
      dica = Dica.find(dica_id)
      
      respond_to do |format|
        format.json { render :json => {:success => true,
                                       :dica => dica}
                                       }
      end
    end
  end
  
   
  def get_ultimas_dicas
      page = params[:page] || 1
      dica = Dica.order_by(:updated_at.desc).limit(MainYetting.DEFAULT_REG_PER_PAGE).skip((page.to_i-1) * MainYetting.DEFAULT_REG_PER_PAGE)
      
      respond_to do |format|
        format.json { render :json => {:success => true,
                                       :dicas => dica,
                                       :reg_pag => MainYetting.DEFAULT_REG_PER_PAGE}
                                     }
      end
    
  end
  
end
