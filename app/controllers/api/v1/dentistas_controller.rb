class Api::V1::DentistasController < ApplicationController

  respond_to :json
  
  #before_action :authenticate_user!
  
  #Retorna dados do bar
  # http://dentistasapp.dev.com/api/v1/dentista/545a5e0c52616e29a8010000.json
  def get
    dentista_id = params[:dentista_id]
    if dentista_id.nil? or dentista_id.empty? then
        respond_to do |format|
          format.json { render :json => {:success => false,
                                         :info => "Parâmetros inválidos!"} }
        end
    else
      dentista = Dentist.find(dentista_id)
      
      respond_to do |format|
        format.json { render :json => {:success => true,
                                       :dentista => dentista.as_json()}
                                       }
      end
    end
  end
  
    
  #Retorna os bares com os pacotes próximos
  # curl http://dentistasapp.herokuapp.com/api/v1/nr_dentistas.json
  def count
    respond_to do |format|
      format.json { render :json => {:success => true,
                                     :nr => Dentist.count} }
    end

  end

  # http://dentistasapp.herokuapp.com/api/v1/dentistas/find_near.json?lat=-23.56033&lng=-46.58301
  def find_filter
    
    cidade = params[:cidade]
    estado = params[:estado]
    bairro = params[:bairro]
    nome = params[:nome]
    servico = params[:servico]
    emergencia = params[:emergencia]
    promo = params[:promo] || params[:promocao]
    lat = params[:lat]
    lng = params[:lng]
    
    page = params[:page]
    if page.nil? then 
      page = "1"
    end
    
    expressions = []
    query = Dentist.all
    expressions.push({ "active" => true})
    expressions.push({ "dt_expiration_date" => {"$gte" => Date.today()}})
    
    if not nome.nil? and not nome.empty? then
      nome = ActiveSupport::Inflector.transliterate(nome)
      nome = make_search_exp(nome)
      query = query.or({:nome => /.*#{nome}.*/i} , {:"equipe" => {"$in" => [/.*#{nome}.*/i]}  } )
      expressions.push({ "$or" => [ {"nome" => /.*#{nome}.*/i}, {"equipe" => {"$in" => [/.*#{nome}.*/i]}  } ] })
    end

    if not cidade.nil? and not cidade.empty? then
      cidade = ActiveSupport::Inflector.transliterate(cidade)
      cidade = make_search_exp(cidade)
      query = query.where("address.city" => /.*#{cidade}.*/i) 
      expressions.push({ "address.city" => /.*#{cidade}.*/i })
    end
    
    if not bairro.nil? and not bairro.empty? then
      bairro = ActiveSupport::Inflector.transliterate(bairro)
      bairro = make_search_exp(bairro)
      query = query.where("address.neighborhood" => /.*#{bairro}.*/i) 
      expressions.push({ "address.neighborhood" => /.*#{bairro}.*/i })
    end
    
    if not servico.nil? and not servico.empty? then
      servico = ActiveSupport::Inflector.transliterate(servico)
      servico = make_search_exp(servico)
      query = query.in("servicos" => [/.*#{servico}.*/i]) 
      expressions.push({ "servicos" =>  {"$in" => [/.*#{servico}.*/i]} })
      
     
    end

    
    if not emergencia.nil? and not emergencia.empty? and emergencia == "true" then
      query = query.exists(:"at_emerg" => true) 
      expressions.push({ "at_emerg" => {"$exists" => true}})
    end
    
    if not promo.nil? and not promo.empty? and promo == "true" then
      query = query.exists(:"promo" => true) 
      expressions.push({ "promo" => {"$exists" => true}})
    end
   
    queryJson = nil
    if not expressions.nil? and expressions.size > 0 then
      queryJson = { "$and" => expressions }
    end

    @dentistas = nil
    if lng.nil? or lng.empty? or lat.nil? or lat.empty? then
      per_page = MainYetting.DEFAULT_REG_PER_PAGE
      skip = ((page.to_i - 1) * per_page)
      @dentistas = Dentist.where(queryJson).limit(per_page).skip(skip)
    else 
      @dentistas = Dentist.find_near( [lng.to_f, lat.to_f], params[:page], queryJson)
    end
    
    #dentistas = Dentist.only(:id, :name, :ph, :pr, :geo_near_distance, :coordinates, :address)
    #            .for_js("this.n = p_nome", p_nome: "#{nome}")
                
    respond_to do |format|
        format.json { render :json => {:success => true, 
                                        :dentistas => @dentistas.as_json( 
                                             :base_info =>  true),
                                        :reg_pag => MainYetting.DEFAULT_REG_PER_PAGE}}
      end
    
  end
  
  #Retorna os bares com os pacotes próximos
  # curl http://dentistasapp.herokuapp.com/api/v1/dentistas/find_near.json?lat=-23.56033&lng=-46.58301
  def find_near

    lat = params[:lat]
    lng = params[:lng]
    page = params[:page]
    
    if params[:page].nil? then 
      page = 1
    else
      page = params[:page].to_i
    end
    
    if lat.nil? or lng.nil? then
        respond_to do |format|
          format.json { render :json => {:success => false,
                                         :info => "Parâmetros inválidos"} }
        end
    else 
      @location = [lng.to_f, lat.to_f]
        
      #   dentistas = Dentista
      #          .only(:id, :nome, :phones, :profile, :geo_near_distance, :coordinates, :address)
      #          .limit(MainYetting.DEFAULT_REG_PER_PAGE_DIST)
      #          .geo_near(@location).max_distance(MainYetting.DEFAULT_KM_FIND_NEAR/6378.137)
      #          .spherical.distance_multiplier(6378.137)
               
      expressions = [] 
      expressions.push({ "active" => true})
      expressions.push({ "dt_expiration_date" => {"$gte" => Date.today()}})
      queryJson = { "$and" => expressions }
    
      dentistas = Dentist.find_near( [lng.to_f, lat.to_f], params[:page], queryJson)

      respond_to do |format|
        format.json { render :json => {:success => true, 
                                        :dentistas => dentistas.as_json(:base_info =>  true), 
                                        :reg_pag => MainYetting.DEFAULT_REG_PER_PAGE}}
      end
    end
  end
  
  def get_cities()
    cidades = Dentist.distinct("address.city")
    respond_to do |format|
      format.json { render :json => {:success => true, 
                                      :values => cidades}}
    end
  end

  def get_neighborhood()
    bairros = Dentist.where("address.city" => params[:city]).distinct("address.neighborhood")
    respond_to do |format|
      format.json { render :json => {:success => true, 
                                        :values => bairros}}
    end
  end

  def get_servicos()
    servicos = MainYettings.SERVICOS
    
    
    respond_to do |format|
      format.json { render :json => {:success => true, 
                                        :values => servicos}}
    end
  end
  
  def make_search_exp(str)
     n = str.downcase.strip.to_s
     n.gsub!("a",    '[aàãá]')
     n.gsub!("e",    '[eéê]')
     n.gsub!("i",    '[ií]')
     n.gsub!("o",    '[oó]')
     n.gsub!("u",    '[uú]')
     n.gsub!("c",    '[cç]')
     n
  end
  
end
