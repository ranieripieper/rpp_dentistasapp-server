class Payment
  include Mongoid::Document
  include Mongoid::Timestamps
  
  before_create :assign_id

  field :_id,             type: Integer
  field :value,           type: Float
  field :type,            type: String
  field :boleto_url,      type: String
  field :transaction_id,  type: String
  
  embeds_many :payment_status
  belongs_to :dentist
  
private 
    def assign_id
      self._id = Sequence.generate_id(:payment)
    end 
end