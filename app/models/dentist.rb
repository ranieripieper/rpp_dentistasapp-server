require 'geocoder'

class Dentist

  include Mongoid::Document
  include Geocoder::Model::Mongoid
  include Mongoid::Timestamps
  include Mongoid::Paperclip
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :timeoutable, :timeout_in => 10.minutes
  
  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time
  
  before_create :assign_id
  before_save :adjust_phone
  
  ## Campos
  field :_id,         type: Integer
  field :nome,        type: String
  field :servicos,    type: Array
  field :equipe,      type: Array
  field :site,        type: String
  field :profile
  #field :pictures,    type: Array
  field :phones,      type: Array
  field :destaque,    type: Integer, default: 1
  field :icon,        type: String
  field :active,      type: Boolean, default: false
  field :dt_expiration_date, type: Date, default: nil
  field :plano,       type: String
  
  embeds_many :pictures, :class_name => "Image", :as => :pictures

  has_mongoid_attached_file :profile, :default_url => "http://dentistasapp.herokuapp.com/images/img_placeholder.png"
  validates_attachment_content_type :profile, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, :attributes => :profile, :less_than => 1.megabytes
  #validates :profile, :dimensions => { :width => 300, :height => 300 }
  
  embeds_many :at_normal,       as: :horarios, store_as: "at_normal"  , class_name: "Horario"
  embeds_many :at_emerg,        as: :horarios, store_as: "at_emerg"   , class_name: "Horario"

  embeds_one :address, as: :addressed
  embeds_one :promocao, as: :promocao, store_as: "promo"  , class_name: "Promocao"
  
  has_many :payments
  
  field :geo_near_distance

  default_scope -> { order_by(:destaque.desc) }
  
  validates :nome, length: { minimum: 8, maximum: 50 }
  #validates :nome, :servicos, :address, presence: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, :allow_blank => true
  validates :site, format: { with: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/ }, :allow_blank => true
      
  #rake db:mongoid:create_indexes
  #db.events.ensureIndex( { "address.coordinates" : "2dsphere" } )
  index({ "address.coordinates" => "2dsphere" }, { name: "coord_index", background: true })
  
  
  def as_json(options = { })
    
    h = super(options)
    if not promocao.nil? and not promocao.servico.nil? and not promocao.perc.nil? then
      h[:has_promo] = true
    else
      h[:has_promo] = false
    end
    if not at_emerg.nil? and not at_emerg.empty? then
      h[:has_emerg] = true
    else
      h[:has_emerg] = false
    end
      
    if not options[:base_info].nil? and options[:base_info] then
      h.delete("at_emerg")
    end
    
    if not pictures.nil? and not pictures.empty? then
      pics = []
      pictures.each do |pic|
        pics << pic.picture.url
      end
      h[:pictures] = pics
    else
      h.delete("pictures")
    end

    h.delete("profile_content_type")
    h.delete("profile_file_name")
    h.delete("profile_file_size")
    h.delete("profile_fingerprint")
    h.delete("profile_updated_at")
    h.delete("updated_at")
    h.delete("created_at")
    
    h.delete("encrypted_password")
    h.delete("reset_password_token")
    h.delete("reset_password_sent_at")
    h.delete("remember_created_at")
    h.delete("encrypted_password")
    h.delete("sign_in_count")
    h.delete("current_sign_in_at")
    h.delete("last_sign_in_at")
    h.delete("current_sign_in_ip")
    h.delete("last_sign_in_ip")

 
    h
  end
  
  def dt_expiration_date_format
    if not dt_expiration_date.nil? then
      return dt_expiration_date.strftime('%d/%m/%Y')
    end
    return ""
  end
  


  attr_accessor :distance
  class << self
    def find_near(near, page=nil, query=nil)
        page = page || 1
        per_page = MainYetting.DEFAULT_REG_PER_PAGE
  
        pipeline = [
          { "$geoNear" => {
            "near" => near,
            "distanceField" => "distance",
            "distanceMultiplier" => 6378.137,
            "maxDistance" => MainYetting.DEFAULT_KM_FIND_NEAR/6378.137,
            "spherical" => true,
            "query" => query }
          }
        ]
  
        count = collection.aggregate(pipeline).count
  
        if page && per_page
          pipeline << { '$skip' => ((page.to_i - 1) * per_page) }
          pipeline << { '$limit' => per_page }
        end
  
        places_hash = collection.aggregate(pipeline)
  
        ids = places_hash.map{|e| e['_id'].to_s}
        places = self.find(ids)
  
        places = inject_distance_and_sort(places, places_hash)
  
        places.instance_eval <<-EVAL
          def current_page
            #{ page }
          end
          def total_pages
            #{ count.modulo(per_page).zero? ? (count / per_page) : ((count / per_page) + 1)}
          end
          def limit_value
            #{ per_page }
          end
        EVAL
  
        places
      end
  end 

  def to_param
    id.to_s
  end

  def phone
    if not self.phones.nil? and not self.phones.empty? then
      return self.phones[0]
    end
  end
  
  private
    # private class methods
    class << self
      def inject_distance_and_sort(places, places_hash)
        places = places.each do |place|
          p = places_hash.select {|p| p["_id"] == place.id}.first
          place.geo_near_distance =  p["distance"]
        end
  
        places.sort_by{|a| [-a.destaque , a.geo_near_distance]}
          
           #|a,b| b.destaque <=> a.destaque, a.geo_near_distance <=> b.geo_near_distance }
      end
    end

    def assign_id
      self._id = Sequence.generate_id(:dentista)
    end  

    
    def adjust_phone
      if not self.phones.nil? then
        tmp = []
        self.phones.each do |phone| 
          phone = phone.gsub("_", "")
          phone = phone.gsub("-", "")
          phone = "#{phone[0..phone.length-4-1]}-#{phone[phone.length-4..phone.length]}"
          tmp << phone
        end
        self.phones = tmp
      end
    end
end