class Promocao
  include Mongoid::Document
  
  field :_id,             type: String, default: nil
  field :servico,         type: String
  field :perc,            type: String

  embedded_in :dentist, :inverse_of => :promocao
 
end