class Image
  include Mongoid::Document
  include Mongoid::Paperclip

  field :_id,         type: Integer

  before_create :assign_id
  embedded_in :dentist, :inverse_of => :pictures

  #belongs_to :dentista, class_name: "Dentista", inverse_of: :picture

  has_mongoid_attached_file :picture, :default_url => "http://placehold.it/200x200"
  
  validates_attachment_content_type :picture, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, :attributes => :picture, :less_than => 1.megabytes

  private
    def assign_id
      self._id = Sequence.generate_id(:dentista_pics)
    end  
    
end