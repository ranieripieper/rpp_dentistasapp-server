class Horario
  include Mongoid::Document
  
  
  @@DOMINGO = 1
  @@SEGUNDA = 2
  @@TERCA = 3
  @@QUARTA = 4
  @@QUINTA = 5
  @@SEXTA = 6
  @@SABADO = 7
  
  field :_id, type: String, default: nil
  field :dia_sem, type: Integer
  field :h_open,  type: String
  field :h_close, type: String
  
  def identify
  end
  
  def serializable_hash(options)
      original_hash = super(options)
      Hash[original_hash.map {|k, v| [self.aliased_fields.invert[k] || k , v] }]
  end

  class << self
    def serialize_from_session(key, salt)
      record = to_adapter.get(key[0]["$oid"])
      record if record && record.authenticatable_salt == salt
    end
  end

  def self.segunda
    @@SEGUNDA
  end

  def self.terca
    @@TERCA
  end

  def self.quarta
    @@QUARTA
  end

  def self.quinta
    @@QUINTA
  end

  def self.sexta
    @@SEXTA
  end

  def self.sabado
    @@SABADO
  end

  def self.domingo
    @@DOMINGO
  end

end