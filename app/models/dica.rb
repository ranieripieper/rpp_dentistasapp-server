class Dica
  include Mongoid::Document
  include Mongoid::Timestamps
  
  before_create :assign_id
  
  field :_id,                                         type: Integer
  field :dica,         as: :dica,                        type: String
  field :link,         as: :link,                        type: String
  belongs_to :dentista, class_name: "Dentist"
  
  field :dentista

  default_scope -> { order_by(:updated_at.desc) }

  def dentista
    if not self.dentista_id.nil? then
      return Dentist.only(:id, :nome, :icon).find(self.dentista_id)
    end
    return nil
  end
  
  private
    def assign_id
      self._id = Sequence.generate_id(:dica)
    end
end