class PaymentStatus
  include Mongoid::Document
  include Mongoid::Timestamps
  
  before_create :assign_id

  field :_id,             type: Integer
  field :status,          type: String
  field :dt,              type: Time

  embedded_in :payment
  
private 
    def assign_id
      self._id = Sequence.generate_id(:payment_status)
    end 
end