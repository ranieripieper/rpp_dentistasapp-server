class DentistaMailer < ActionMailer::Base
  default from: "contato@dentistasapp.com"

  
  def payment_process(dentista, payment)
    @dentista = dentista
    @payment = payment
    mail(to: @dentista.email, subject: MainYetting.EMAIL_SUBJECT_PAYMENT_PROCESS)
  end

  def update_status(dentista, payment, status)
    @dentista = dentista
    @payment = payment
    @status = status
    mail(to: @dentista.email, subject: MainYetting.EMAIL_SUBJECT_PAYMENT_STATUS_PROCESS)
  end
  
  def payment_refused(dentista, payment, status)
    @dentista = dentista
    @payment = payment
    @status = status
    mail(to: @dentista.email, subject: MainYetting.EMAIL_SUBJECT_PAYMENT_REFUSED)
  end
  
  def payment_confirmed(dentista, password)
    @dentista = dentista
    @password = password
    mail(to: @dentista.email, subject: MainYetting.EMAIL_SUBJECT_PAYMENT_CONFIRMED)
  end
    
end
