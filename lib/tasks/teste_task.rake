# encoding: UTF-8

require 'rubygems'
require 'open-uri'
require 'geocoder'
require 'rspec'
require 'iconv'

#require File.join(File.dirname(__FILE__), '/generate_capa')
desc "Executa todas as tasks"

task :teste_task => :environment do

      transaction = nil
      transaction = PagarMe::Transaction.new({
          :amount => 100,
          :soft_descriptor => MainYetting.TEXTO_FATURA_CARTAO,
          :card_hash => "#{params["card_hash"]}",
          :postback_url => MainYetting.PAYMENT_UPDATE_STATUS_URL
      })

      transaction.charge
      
      puts "transaction.id = #{transaction.id}"
      puts "transaction.status = #{transaction.status}"
end