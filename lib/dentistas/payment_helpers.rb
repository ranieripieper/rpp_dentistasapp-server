module Dentistas
  module PaymentHelpers
    extend self
    
    def payment(dentista, value, params)
      PagarMe.api_key = MainYetting.PAGAR_ME_API_KEY
  
      #cria payment
      payment = Payment.new
      payment.dentist = dentista
      payment.value = value
      transaction = nil
      if params[:payment_type] == MainYetting.PAYMENT_BOLETO then
        transaction = PagarMe::Transaction.new({
            :amount => value,
            :payment_method => "#{MainYetting.PAYMENT_BOLETO}",
            :postback_url => "#{MainYetting.PAYMENT_UPDATE_STATUS_URL}"
        })
      else 
        transaction = PagarMe::Transaction.new({
            :amount => value,
            :soft_descriptor => "#{MainYetting.TEXTO_FATURA_CARTAO}",
            :card_hash => "#{params["card_hash"]}",
            :postback_url => "#{MainYetting.PAYMENT_UPDATE_STATUS_URL}"
        })
      
      end
      
      puts ">>>>> #{MainYetting.PAYMENT_UPDATE_STATUS_URL}"
      transaction.charge
      
      payment_status = PaymentStatus.new
      payment_status.dt = Time.now
      payment.transaction_id = transaction.id
      payment_status.status = transaction.status # status da transação
      payment.payment_status << payment_status
      payment.type = params[:payment_type]
      if params[:payment_type] == MainYetting.PAYMENT_BOLETO then
        payment.boleto_url = transaction.boleto_url # URL do boleto bancário  
      end
      
      payment.save!
      
      payment
    end
    
    def get_value(plano)
      value = MainYetting.VALOR_SEMESTRAL
      if plano == MainYetting.PLANO_ANUAL then
        value = MainYetting.VALOR_ANUAL
      end
      return value
    end
  end 
  

end