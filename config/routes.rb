Rails.application.routes.draw do

  root :to =>  'site#home_new'
  get  '/:name/:id.html'          => 'site#home_new', :as => :site_home_dentista

 # root :to => "static#index_for_now"
  
  devise_for :dentists, path: "dentista", 
    controllers: { sessions: "dentist/sessions", registrations: 'dentist/registrations' },   
    path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', 
        unlock: 'unlock', registration: 'register', sign_up: 'sign_up' } do
    
    authenticated :dentists do
      root 'dentists/registrations#edit', as: :authenticated_root #'admin#index', as: :authenticated_root
    end
  
    unauthenticated do
      root 'dentists/sessions#new', as: :unauthenticated_root
    end
  end
  
  get  'dentista/payment/process/:dentista_id'          => 'dentist/payment#process_payment', :as => :payment_process_payment
  post  'dentista/payment/update_status'   => 'dentist/payment#update_status', :as => :payment_update_status
  get  'dentista/payment/redo_payment/:dentista_id'   => 'dentist/payment#redo_payment', :as => :redo_payment
  patch  'dentista/payment/process_redo_payment'   => 'dentist/payment#process_redo_payment', :as => :process_redo_payment

  devise_for :users
  
  devise_scope :user do
    authenticated :user do
      root 'admin/admin#index', as: :authenticated_root #'admin#index', as: :authenticated_root
    end
  
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
  
  namespace :api do
    namespace :v1 do
     
      get  'dentista/:dentista_id'                    => 'dentistas#get'
      get  'dentistas/find_near'                      => 'dentistas#find_near'
      get  'dentistas/cidades'                        => 'dentistas#get_cities'
      get  'dentistas/bairros'                        => 'dentistas#get_neighborhood'
      get  'dentistas/servicos'                       => 'dentistas#get_servicos'
      get  'dentistas/find_filter'                    => 'dentistas#find_filter' 
      get  'nr_dentistas'                             => 'dentistas#count'

      get  'dica/get_all'                             => 'dicas#get_ultimas_dicas' 
      
    end
  end
   
   
  namespace :admin do
    get '' => 'admin#index' 
    get 'admin/index'
    
    #dentista
    get     'dentista' => 'dentista#index'
    get     'dentista/new'
    post    'dentista/create'
    get     'dentista/:id' => 'dentista#show', as: 'dentista_show'
    get     'dentista/:id/edit' => 'dentista#edit', as: 'dentista_edit'
    patch   'dentista/:id' => 'dentista#update', as: 'dentista_update_patch'
    put     'dentista/:id' => 'dentista#update', as: 'dentista_update_put'
    delete  'dentista/:id' => 'dentista#destroy', as: 'dentista_delete' 
    delete     'dentista/foto/:id/:id_image' => 'dentista#delete_foto', as: 'dentista_delete_foto' 
    
    #dica
    get     'dica' => 'dica#index'
    get     'dica/new'
    post    'dica/create'
    get     'dica/:id' => 'dica#show', as: 'dica_show'
    get     'dica/:id/edit' => 'dica#edit', as: 'dica_edit'
    patch   'dica/:id' => 'dica#update', as: 'dica_update_patch'
    put     'dica/:id' => 'dica#update', as: 'dica_update_put'
    delete  'dica/:id' => 'dica#destroy', as: 'dica_delete'
    
  end
end
